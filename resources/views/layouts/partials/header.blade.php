<header>
    <h1>Librería digital</h1>
    <nav>
        <ul>
            <li><a href="{{route('home')}}" class="{{request()->routeIs('home') ? 'active' : 'inactive'}}">Home</a>
            </li>
            <li><a href="{{route('libros.index')}}" class="{{request()->routeIs('libros.*') ? 'active' : 'inactive'}}">Libros</a>
            </li>
            <li><a href="{{route('nosotros')}}" class="{{request()->routeIs('nosotros') ? 'active' : 'inactive'}}">Nosotros</a>
            </li>
        </ul>
    </nav>
</header>

@extends('layouts.plantilla')

@section('title', 'Cursos index')

@section('content')
    <h1 class="flex text-lg justify-center items-center h-full">Bienvenido a la lista de libros</h1>
    <a class="ml-10 text-lg" href="{{route('libros.create')}}">Añadir libro</a>
    <ul>
        @foreach ($libros as $libro)
            <li>
                <a href="{{route('libros.show', $libro)}}">{{$libro->titulo}}</a>
            </li>
        @endforeach
    </ul>
    {{$libros -> links()}}
@endsection

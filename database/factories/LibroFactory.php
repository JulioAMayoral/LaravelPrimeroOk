<?php

namespace Database\Factories;

use App\Models\Libro;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Libro>
 */
class LibroFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Libro::class;
    public function definition(): array
    {
        return [
            'titulo' => $this->faker->sentence(),
            'slug'=> $this->faker->slug(),
            'autor' => $this->faker->name(),
            'isbn' => $this->faker->isbn10()
        ];
    }
}

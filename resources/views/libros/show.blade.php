@extends('layouts.plantilla')

@section('title', 'Cursos show' . $libro->titulo)

@section('content')
    <h1>Bienvenido a la lista de libros: {{$libro->titulo}}</h1>
    <p>Autor: {{$libro -> autor}}</p>
    <p>ISBN: {{$libro -> isbn}}</p>
    <a href="{{route('libros.edit', $libro)}}">Editar libro</a><br>
    <form action="{{route('libros.destroy', $libro)}}" method="POST">
        @csrf
        @method('delete')
        <button type="submit">Eliminar libro</button>
    </form>
    <a href="{{route('libros.index')}}">Volver al inicio</a>
@endsection

<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCurso;
use App\Http\Requests\StoreLibro;
use App\Models\Libro;
use Illuminate\Http\Request;

class LibroController extends Controller
{
    public function index(){

        $libros = Libro::orderBy('id','desc')->paginate(15);

        return view("libros.index", compact("libros"));
    }

    public function create(){
        return view("libros.create");
    }

    public function store(StoreLibro $request){

        // $libro = new Libro();

        // $libro->titulo = $request->titulo;
        // $libro->autor = $request->autor;
        // $libro->isbn = $request->isbn;

        // $libro -> save();

        //Lo guarda automaticamente

        /*
        Eso equivale a esto
        $libro = Libro::create([
            'titulo' => $request->titulo,
            'autor'=> $request->autor,
            'isbn'=> $request->isbn
        ]);
        Y más corto aún:
        */
        $libro = Libro::create($request->all());

        return redirect()->route("libros.show", $libro);
    }

    public function show(Libro $libro){
        //compact("categoria"); //['categoria' => $categoria]
        //$libro = Libro::find($libro);
        return view("libros.show", compact("libro"));
    }

    public function edit(Libro $libro){
        return view ("libros.edit", compact("libro"));
    }

    public function update(Request $request, Libro $libro){

        $request->validate([
            "titulo"=> "required|min:3",
            "slug"=> "required|unique:libros" . $libro->id,
            "autor"=> ["required","min:4"],
            "isbn"=> "required"
        ]);

        // $libro->titulo = $request->titulo;
        // $libro->autor = $request->autor;
        // $libro->isbn = $request->isbn;

        // $libro -> save();

        $libro -> update($request->all());

        return redirect()->route("libros.show", $libro);
    }

    public function destroy(Libro $libro){
        $libro -> delete();

        return redirect()->route("libros.index");
    }
}

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LibroController;


Route::get('/', HomeController::class)->name('home');
Route::resource('libros', LibroController::class);
Route::view('nosotros', 'nosotros')->name('nosotros');

// Route::controller(LibroController::class)->group(function () {

//     Route::get('libros', 'index')->name('libros.index');

//     Route::get('libros/create', 'create')->name('libros.create');

//     Route::post('libros', 'store')->name('libros.store');

//     Route::get('libros/{libro}','show')->name('libros.show');

//     Route::get('libros/{libro}/edit','edit')->name('libros.edit');

//     Route::put('libros/{libro}', 'update')->name('libros.update');

//     Route::delete('libros/{libro}', 'destroy')->name('libros.destroy');
// });

/*
    Route::get('libros', [LibroController::class,'index']);
    Route::get('libros/create', [LibroController::class, 'create']);
    Route::get('libros/{categoria}/{autor?}', [LibroController::class,'show']);
*/

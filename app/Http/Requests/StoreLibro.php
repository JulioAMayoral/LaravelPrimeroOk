<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLibro extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            "titulo"=> "required|min:3",
            "slug"=> "required|unique:libros",
            "autor"=> ["required","min:4"],
            "isbn"=> "required"
        ];
    }

    public function messages(): array
    {
        return [
            'titulo.required' => 'El libro debe llevar un título',
            'autor.required' => 'El autor no puede estar vacío',
            'isbn.required' => 'El ISBN es imprescindible'
        ];
    }
}

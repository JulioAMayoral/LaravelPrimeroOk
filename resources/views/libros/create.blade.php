@extends('layouts.plantilla')

@section('title', 'Cursos create')

@section('content')
    <h1 class="flex justify-center items-center h-full">Página para poder introducir un libro</h1>
    <form action="{{route('libros.store')}}" method="POST" class="bg-cyan-400 w-80 mx-auto mt-8 rounded-lg p-6">

        @csrf

        <label>
            Título:
            <input class="border border-gray-300 w-full px-3 py-2 mb-2 rounded-md disabled:bg-red-200" type="text"
            name="titulo" value="{{old('titulo')}}">
        </label>

        @error('titulo')
            <br>
            {{$message}}
            <br>
        @enderror

        <label>
            Slug:
            <input class="border border-gray-300 w-full px-3 py-2 mb-2 rounded-md disabled:bg-red-200" type="text"
            name="slug" value="{{old('slug')}}">
        </label>

        @error('slug')
            <br>
            {{$message}}
            <br>
        @enderror

        <label>
            Autor:
            <input class="border border-gray-300 w-full px-3 py-2 mb-2 rounded-md disabled:bg-red-200" type="text"
            name="autor" value="{{old('autor')}}">
        </label>

        @error('autor')
            <br>
            {{$message}}
            <br>
        @enderror

        <label>
            ISBN:
            <input class="border border-gray-300 w-full px-3 py-2 mb-2 rounded-md disabled:bg-red-200" type="text"
            name="isbn" value="{{old('isbn')}}">
        </label>

        @error('isbn')
            <br>
            {{$message}}
            <br>
        @enderror

        <input class="bg-blue-500 w-full py-2 text-white rounded-md cursor-pointer hover:bg-blue-400" type="submit"
            value="Añadir libro">
    </form>
@endsection
